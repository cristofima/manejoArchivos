@extends('layouts.app')
@section('title','Registrar producto')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <p><a href="{{url('archivos')}}"><button class="btn btn-success">Regresar a Listado</button></a></p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar archivo</div>
                <div class="card-body">
                    {!! Form::open(['url' => 'archivos','files'=>'true']) !!}
                    <div class="form-group row">
                        <label for="arc_nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>
                        <div class="col-md-6">
                            <input type="text"  class="form-control{{ $errors->has('arc_nombre') ? ' is-invalid' : '' }}" name="arc_nombre" id="arc_nombre" value="{{old('arc_nombre')}}">
                            @if ($errors->has('arc_nombre'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('arc_nombre') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="imagen" class="col-md-4 col-form-label text-md-right">Imagen</label>
                        <div class="col-md-6">
                            <input type="file"  class="form-control{{ $errors->has('imagen') ? ' is-invalid' : '' }}" name="imagen" id="imagen">
                            @if ($errors->has('imagen'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('imagen') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
