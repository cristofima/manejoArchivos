@extends('layouts.app')
@section('title','Archivos')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <p><a href="{{url('archivos/create')}}"><button class="btn btn-success">Nuevo</button></a></p>
        </div>
    </div>
    @include('includes.mensajes')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Archivos</div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table  class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Imagen</th>
                                        <th scope="col">Fecha Creación</th>
                                        <th scope="col">Ultima Actualización</th>
                                    </tr>
                                </thead>
                               @foreach ($archivos as $a)
                                <tr>
                                    <td>{{$a->arc_nombre}}</td>
                                    <td>
                                        @if($a->arc_contenido!=null)
                                        <img src="data:{{$a->arc_mime_type}};base64,{{stream_get_contents($a->arc_contenido)}}" alt="Imagen" style="max-width: 75px;max-height: 75px;"/>
                                        @else
                                            {{'No hay imagen'}}
                                        @endif
                                    </td>
                                    <td>{{$a->created_at}}</td>
                                    <td>{{$a->updated_at}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$archivos->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#modalDelete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var action = button.data('action');
            var name = button.data('name');
            var modal = $(this);
            modal.find(".modal-content #txtEliminar").text("¿Está seguro de eliminar el producto " + name + "?");
            modal.find(".modal-content form").attr('action', action);
        });
    });
</script>
@endsection
