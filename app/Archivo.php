<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $table="archivos";
    protected $primaryKey="arc_id";
    public $timestamps=true;

    protected $fillable=['arc_nombre','arc_mime_type','arc_contenido'];
}
