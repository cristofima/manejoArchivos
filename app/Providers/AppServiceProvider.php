<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Archivo;
use Input;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        $this->eventosModelos();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function eventosModelos(){
        Archivo::creating(function ($arc) {
            if(Input::hasFile('imagen')){
                $image = Input::file('imagen');
                $arc->arc_contenido=base64_encode(file_get_contents($image->getRealPath()));
                $arc->arc_mime_type= $image->getMimeType();
            }
        });
        Archivo::updating(function ($arc) {
            if(Input::has('arc_id')){
                $id=Input::get('arc_id');
                $archivo=Archivo::findOrFail($id);

                if(Input::hasFile('imagen') && $arc->imagen!=null){
                    $image = Input::file('imagen');
                    if($image!=null){
                        $arc->arc_contenido=base64_encode(file_get_contents($image->getRealPath()));
                        $arc->arc_mime_type= $image->getMimeType();
                    }
                }else{
                    $arc->arc_contenido=stream_get_contents($archivo->arc_contenido);
                    $arc->arc_mime_type= $archivo->arc_mime_type;
                }
            }
        });
    }
}
