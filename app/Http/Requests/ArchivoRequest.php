<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Input;

class ArchivoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Input::has('arc_id')){
            return [
                'arc_nombre'=>'required | string | max:10',
                'imagen' => 'nullable|file|mimes:jpeg,png,jpg,bmp,JPG|dimensions:min_width=300,min_height=300,max_width=2000,max_height=2000|max:2048'
            ];
        }else{
            return [
                'arc_nombre'=>'required | string | max:10',
                'imagen' => 'required|file|mimes:jpeg,png,jpg,bmp,JPG|dimensions:min_width=300,min_height=300,max_width=2000,max_height=2000|max:2048',
            ];
        }
    }
}
