<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use App\Http\Requests\ArchivoRequest;
use Illuminate\Support\Facades\Redirect;

class ArchivoController extends Controller
{
    public function index()
    {   
        $archivos=Archivo::paginate(10);
        return view('archivos.index',compact('archivos'));
    }

    public function create(){
        return view('archivos.create');
     }

    public function store(ArchivoRequest $request){
        Archivo::create($request->all());
        return Redirect::to('archivos')->with('success', 'Archivo registrado');
    }

    public function edit($id) {
        $arc=Archivo::findOrFail($id);
        return view('archivos.edit',compact('arc'));
    }

    public function update(ArchivoRequest $request, $id){
        Archivos::updateOrCreate(['arc_id'=>$id],$request->all());
        return Redirect::to('archivos')->with('success', 'Archivo actualizado');
    }

    public function destroy($id){
        Archivos::destroy($id);
        return Redirect::to('archivos')->with('success', 'Archivo eliminado');
    }
}
